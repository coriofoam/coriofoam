#!/bin/bash

foamListTimes -rm -withZero
foamCleanPolyMesh
rm -f system/blockMeshDict
rm -f log.txt tmp_*.oar
rm -rf processor*
rm -f OAR.*.stderr OAR.*.stdout
