#!/usr/bin/env bash

NBCORES=$(grep numberOfSubdomains system/decomposeParDict |\
		 awk '{print $2}' | cut -f 1 -d ";")

DATE=$(date +%Y%m%d_%H%M%S)

TMP_NAME=tmp_${DATE}_$$.oar

echo We are going to use the temporary file $TMP_NAME.

sed "s/__NBCORES__/${NBCORES}/g" template.oar > $TMP_NAME

chmod u+x $TMP_NAME

oarsub -S ./$TMP_NAME
