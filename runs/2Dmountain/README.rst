Tutorial spinup_strat
=====================

Different parameters can be modified in some files, in particular:

- constant/polyMesh/blockMeshDict
- system/funkySetFieldsDict
- system/decomposeParDict

Do not modify the files in the directories coriofoam/tutorials/ unless you
really know what you do. When you want to modify the parameters, copy the
tutorial directory in the directory coriofoam/runs with a proper name (for
example with the date and time) and then modify these files.

Depending on how you want to run the simulation, different commands have to be
launched. The commands are in bash files corresponding to different steps:

- clean.sh
- init_simul.sh
- init_parallel.sh (only for parallel runs)
- launch_sequencial.sh
- launch_parallel_local.sh
- launch_parallel_cluster.sh

Before running these files, have a look at the code and the explanations in the
file. You can use `cat` like this::

  cat clean.sh

To init and launch a sequential simulation::

  ./clean.sh
  ./init_simul.sh
  ./launch_sequencial.sh

For parallel simulations, first write a correct number of cores in the file
system/decomposeParDict (line `numberOfSubdomains 16;`)

To run a parallel simulation on your local machine (Do not do this on the
frontal of the cluster!)::

  ./clean.sh
  ./init_simul.sh
  ./init_parallel.sh
  ./launch_parallel_local.sh

To run a parallel simulation on the cluster::

  ./clean.sh
  ./init_simul.sh
  ./init_parallel.sh
  ./launch_parallel_cluster.sh
