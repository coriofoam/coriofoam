# Projet CORIOFOAM: Simulating flows in the Coriolis platform with OpenFOAM

## Get the repository on your local computer

First [setup Mercurial with
`hg-git`](https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html).
Then the repository can be cloned with:

```sh
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:coriofoam/coriofoam.git
```

## Install

To install, run the command:

```sh
make
```

and follow the instructions given by the script (read!).

## Tutorials

To run the tutorials, see the next README:

```sh
cd tutorials
cat README.rst
```
