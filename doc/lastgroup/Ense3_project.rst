Ense3 project
=============



Basic commands
--------------

Here you can read all the basic commands we used. PLEASE, use titles in order
to precise why these basic commands are used.


Shortcuts in Linux
~~~~~~~~~~~~~~~~~~

- Ctrl + Alt + t : loads the terminal 

- Alt +Tab : "change the current window"

- Ctrl + Shift + t : opens a new consol in the same directory 
  as the original one 

- ls -a : Displays all the files even the hidden files 

- Ctrl +C : Kills the process in the terminal 

Basic commands in Linux
~~~~~~~~~~~~~~~~~~~~~~~

- cp -r name1 name2 : copies and renames the new file
- cp -r name1 file1 : copies a file into a folder
- mv -r position1 position2 : moves file from one directory to another
- rm -r name1 : deletes a file/folder
- cat name1 : show the written content of the file
- pwd : shows current directory

Helful debugging tricks
~~~~~~~~~~~~~~~~~~~~~~~

Access
^^^^^^
- chmod u+x name1 : if permission is denied for a command (life saver !)
- chmod ug+ug name1 : if permission is denied to open a file 
- ls -l : to see the accesses (r for read, w for write)

Simulations
^^^^^^^^^^^
- oarstat -u : if no simulation is launched it will do nothing
- oardel jobnumber : deletes a job 
- ls processor* : to see the advancement of the simulation

First commands
~~~~~~~~~~~~~~

First we have to make sure that whenever we open the terminal, python and
openFOAM are loaded. To do this, we need to change the .bashrc file in the home
directory.

We start first by editing the .bashrc file with gedit::

  gedit .bashrc &

Then we write at the end of the file the two following lines::

  module load openfoam/4.1 
  module load python


IPython 
~~~~~~~

Launch and use IPython
^^^^^^^^^^^^^^^^^^^^^^


First you to load python (if it is not already done)::
  module load python 

Then to load the environment you use the command::
  ipython 

To run a script, you use the command::
  run filename.py

NB: you can use the command ls inside the ipython consol to see the folders you
can access.

To execute a bash command in the terminal without exiting Ipython you use !::

  !cat README.txt

Plot results
^^^^^^^^^^^^

Think about "hg pull" and "hg up" in the pyof directory tu make sure that all
the files are updated

Pour tracer les profils de pression (par exemple) avec matplotlib, il faut
modifier et executer le script : plot _ crossection.py dans les sous dossiers
de ../tutorials .

Comments on the file "plot _ crosssection.py" :

You MUST execute the commands written in pink at the beginning
(ipython --matplotlib for example) before running the file.

When you launch the following command : reconstructPar -time X (Where X is the
time at which we want to see the results) Python creates a folder named X in
the subtutorial directory /grav _ which contains the reconstructed results at
time. Python will use this folder to plot the results at time X.


xm, ym, zm = coordonates of the center of the mesh's cells.

ym _ unique enables to keep only one exemplar of xm, ym, zm (to avoid repetitions)
ym _ unique.sort() orders the values 
dy gives the maximum difference between two centers of cells in the mesh

y0 = 0. gives the plan where we want to plot [[ it is not sure that there are
points of the mesh here ]]

y0 = ym _ unique[abs(ym _unique - y0).argmin()] gives the points of the mesh
which are the nearest y0 = 0.

cond = (y0 - dy < ym) & (ym < y0 + dy) gives an interval among the y direction
to get several points because the mesh is not cartesian (cylindral regions in
the mesh)

key = 'grandeur' to change what we want to visualize 

timename = 'X' corresponds to the folder where the the results at time X have
been stored (the folder is located in the processor folder in the
subdirectories of ../tutorials).


Use the cluster for calculation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For heavy calculation, we have to use the cluster. To do this, 
we have to log to the central computer ????? through  the 
platform X2GO. We first have to launch X2GO and double-clic on 
the new session window. The new session window will require 
some informations which are the following : 

- Host : nrj1sv223
- indentifiant : your loging
- session : XFCE

Don't forget to change the bash.rc on the new window (X2GO). 
You will have to follow the steps presented in the fith paragraphe to launch the simulation. 

How to run a simulation  
~~~~~~~~~~~~~~~~~~~~~~~

- Step 1 : compiling the code

The first step when we want to run the simulation is to compile the code. To do
this we have to go the the directory containing the code of the solver and
execute the command::

  make 

For more help go to .../lastname/coriofoam/README.srt

- Step 2 : launching the simulation 

You just have to follow the instructions contained in the README.srt
(.../lastname/coriofoam/tutorials/nameoftutorial/README.srt)file contained in
the simulation explaining the different steps to launch the simulation. If you
want to make a simulation in the cluster, the .sh files already contain the
commands to launch the calculus with oarsub.

If you want to launch the simulation using more than 4 cores, you have to
change the the decomposeParDict file in the ../tutorials/grav../system. You
open the file with gedit, then you change the field:
 
  numberOfSubdomains 'X'; where X is the number of cores we want to use. 

- Step 3 : 

Once the calculation has been launched, some files and directories will appear
 in the /tutorials/gravitational _ ....  . The processors directories


0_init : contains the files ccx ccy ccz which contain the "coordinates" of the
centers of the mesh

cells.

How to share your files with the others
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First mandatory steps
^^^^^^^^^^^^^^^^^^^^^

First of all you have to make an hg status to see the differences between the
common deposit and your own deposit. To make sur that you have the latest
version of the documents on the common deposit, you have to make an hg
pull. Then you have to make an hg update to update your files and change them
to the latest version of the common deposit. You finish by using the command hg
status, to make everything went all good.

It is important to do it in this order even if you only want to push your own
file, because it avoids us to use the command hg merge.

You have to update to the latest version the folder coriofoam and pyof !


To push your files in the common deposit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You have to use the commands hg commit, hg push.

Be careful when you use the command hg push. You have to check that there are
note unnecessary

files that will be pushed to the common depository like simulation outputs
(appears like a long red

list), if so make sur to delete them before using hg push.

To summarise the steps :

1. hg status

2. hg pull

3. hg update

4. hg status

   (4,5. hg add filename -> to add a file in the depository  that doesn't already exist)

5. hg commit -m "file XXX added" (fait une photo/ copie de votre dossier)

6. hg push

7. hg satus


Useful commands
^^^^^^^^^^^^^^^

In order to see the difference between your file and the common depository file
you can use the command::

  hg diff

To undo what you just did::

  hg revert

To see the changes in a file::

  hg heads

En cas de problème::

  hg merge
  hg resolve


How to use firefox
~~~~~~~~~~~~~~~~~~

To open your file in the website you have to enter the following command in the
terminal :

firefox _build/html/index.html &


Other commands
~~~~~~~~~~~~~~

To kill a process::

  pkill firefox







Session report
--------------

Session 1 : 08/02/2017
~~~~~~~~~~~~~~~~~~~~~~

Today morning, we have learnt to manipulate python and write 
into the terminal with python language (thanks to command ipython). 
In order to do this, we need to modify the bash.rc file in adding 
at the end : `module load openfoam/4.1` et "module load python"

We ran a tutorial code on the local computer,
the PC crashed because we needed more processor.
That's why we moved to the cluster for computation.
The tutorial code ran with success
and we saw that the results of the coriolis modelisation
could be improved (example : gravitational instability),
especially the mesh.
We saw many complicated things that have to be precised.
That's why we defined our objectives this morning
for today and the next sessions.
We have to: 

- write documentation for us, of the basics commands and paths
  (tree structure) in order to work quickly (sara)

- write documentation on the general functioning
  of the tutorial code (understand what the solver did
  to resolve the problem,
  in order to see what can be improved in the code)
  We didn't read it today, the next time we'll start.

- writing python script in order to modify the mesh
  (finished, it worked, detailed in the mesh documentation) 
  (Nicolas)

- create an internet site in order to put in 
  common what we did and make a report for each session (julien)

- exploration of the tutorial commands and entries
  definition of the initial data, time control
  We started to explain the openfoam commands (sav et emere)

for the next sessions : try to understand 
deeply the codes and the solver in order to start modifying it.

Session 2 : 15/02/2017
~~~~~~~~~~~~~~~~~~~~~~

Today morning, Savanah finished her work on the tutorials
After she heldped nicolas on the mesh python program
emre and I Tried to understand the solver code , very difficult,
we need explanations on the Pimple method next session.
We have to understand physical equations and write documentation on it

We had explanations from joel the platform responsible
he will give us experience results next time.
We had a course on the boundary layer.

For the next session : run calculation for the spinup case for different viscosity
and understand the code

Session 3 : 01/03/2017
~~~~~~~~~~~~~~~~~~~~~~

We tried to launch simulation, but we didn't succeed, we had to move to greener for 10 o'clock 
The objectives are reported to the next session.
 

Session 4 : 08/03/2017
~~~~~~~~~~~~~~~~~~~~~~

Today, we ran the spinup stratification code and we obtained some results, 
we need more results and applications in order to analyse it. 
We'll see the analysis for the next session. We took some picture of our results for our presentation. (Savannah, Nicolas)

Emre create a precise GANTT planning 

Sara and Julien worked on the solver and tried to understand the PIMPLE method, 
and wrote the mathematical equations, they did it for U.


Session 5 : 15/03/2017
~~~~~~~~~~~~~~~~~~~~~~

Savannah and Nicolas treated the previous simulation results from the last session. 
The results are in the file "data" and the simulations are in the file "Run".
They used paraview in order to see the results. Savannah and nicolas launched an other simulation with a different viscosity.

Sara, Emre and Julien worked on the equations in the solver. Julien Chauchat gave us explanations 
on the SIMPLE algorithm and gave us the main equations that are solved by the algorithm.
Pierre told us we have to calculate the profile of the free surface in the case of a solid rotation and compare 
to experimental results for different times. We have to do this in two cases : startified flow and homogenous flow. We also have to make an estimation of the time it takes to see a solid rotation appear in the experiment. This time is a caracteritic time. 

