what is CorioFOAM?
==================


CorioFOAM is an OpenFOAM solver adapted to the Coriolis Platform. 

Coriolis platform
-----------------

The Coriolis Platform is the largest experimental rotating platform in the world located in the LEGI lab (Laboratory of Geophysical and Industrial Flow). 

The Coriolis platform is used for the simulation of ocean/meteorological phenomenons, geophysical flows and the study of the Coriolis force.

Its main activity is the experimental modeling of geophysical flows and oceanographical and meteorological phenomenons. 

It is 13 meters wide tank which allows it to take into account the Coriolis force that results from the rotation of the Earth.
 
Indeed the large size of the tank provides access to the inertial regimes that characterize ocean dynamics, with little influence of viscosity and centrifugal force. 

Various changes to the fluid can be made : there can be density stratification or even added topography. 

Laboratory experiments can thus provide a support to model ocean dynamics and develop their physical parameterizations. 

The measuring equipment includes ultrasonic velocity meters, profilers for salinity and temperature. 

Measures are increasingly focused on digital image processing. 

Concentration fields are obtained by laser fluorescence (LIF), and velocity fields by image correlation (PIV). 

These techniques allow three-dimensional measurements in volume by a scanning system of the laser plane.

But they are limited in scale.



OpenFOAM
--------

OpenFOAM (for "Open source Field Operation And Manipulation") is a C++ toolbox for the development of customized numerical solvers, and pre-/post-processing utilities for the solution of continuum mechanics problems, including computational fluid dynamics (CFD). 

The code is released as free and open source software under the GNU General Public License. 

CorioFOAM is no more, no less a solver developped with OpenFOAM.

Thanks to CorioFOAM, we can run simulations. We used it through several `tutorials <file:///.fsdyn_people/rogivue8sa/project/17CORIOFOAM/rogivue/coriofoam/doc/_build/html/tutorials.html>`_ 







