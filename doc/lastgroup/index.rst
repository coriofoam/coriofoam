.. coriofoam documentation master file, created by
   sphinx-quickstart on Mon Jan 30 15:55:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Coriofoam documentation
=======================

Simulating flows in the `Coriolis platform <http://www.legi.grenoble-inp.fr/web/spip.php?rubrique10&lang=en>`_  with OpenFOAM. Tutorials are proposed and explained. The solver is detailed 


.. image:: figs/coriolis_platform.jpg




.. toctree::
   :numbered:
   :maxdepth: 2

   what_is_CorioFOAM

   Tutorials

   solver

   mesh

   Boundary_Conditions
   
   Ense3_project


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
