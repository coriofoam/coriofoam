#!/usr/bin/env python

import os
from math import sin, cos, pi, sqrt
from runpy import run_path

keys_input = ["length", "width", "height", "divx", "divy", "divz"]


path_in = os.path.join("system", "input_make_blockMeshDict.py")
if not os.path.exists(path_in):
    raise ValueError("No file " + path_in)

d = run_path(path_in, {})

for key in keys_input:
    if key not in d:
        raise ValueError(
            "The variable " + key + "has to be defined in the file " + path_in
        )

default_params = {"scale": 1, "ygrading": 1}

for key, value in default_params.items():
    if key not in d:
        d[key] = value

foam_v = os.environ["WM_PROJECT_VERSION"]
txt = f"""
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  {foam_v}                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
"""
txt += """
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
"""

txt += """
scale   {k};

vertices
(
    ( 0.0 0.0 0.0) // Vertex fiveoclocksqb = 0
    ( {a} 0.0 0.0) // Vertex fiveoclockcb = 1
    ( {a} 0.0 {b}) // Vertex fiveoclockcb = 2

    ( 0.0 0.0 {b}) // Vertex fiveoclocksqb = 3
    ( 0.0 {h} 0.0) // Vertex fiveoclockcb = 4
    ( {a} {h} 0.0) // Vertex fiveoclockcb = 5
    ( {a} {h} {b}) // Vertex fiveoclockcb = 6
    ( 0.0 {h} {b}) // Vertex fiveoclockcb = 7

);


""".format(
    b=d["width"], h=d["height"], a=d["length"], k=d["scale"]
)


txt += """
blocks
(
    //square block
    hex (0 1 5 4 3 2 6 7) ({divx} {divy} {divz}) simpleGrading (1 {ygrading} 1)
);

""".format(
    divx=d["divx"], divy=d["divy"], divz=d["divz"], ygrading=d["ygrading"]
)

txt += """
boundary
(
    top
    {
        type wall;
        faces
        (
            (4 5 6 7)
        );
    }

    bottom
    {
        type wall;
        faces
        (
            (0 1 2 3)
        );
    }

    outlet
    {
        type cyclic;
        neighbourPatch inlet;
        faces
        (
            (1 5 6 2)
        );
    }

    inlet
    {
        type cyclic;
        neighbourPatch outlet;
        faces
        (
            (0 3 7 4)
        );
    }

    frontandbackplanes
    {
        type empty;
        faces
        (
            (3 2 6 7)
	    (0 1 5 4)
        );
    }
);

// ************************************************************************* //
"""

print(txt)


path = os.path.join("system", "blockMeshDict")
print("save new blockMeshDict in path\n" + path)

with open(path, "w") as f:
    f.write(txt)
