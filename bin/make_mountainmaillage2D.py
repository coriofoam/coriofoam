#!/usr/bin/env python

import os
from math import sin, cos, pi, sqrt
from runpy import run_path

keys_input = [
    "length",
    "width",
    "height",
    "mount_h",
    "mount_pos",
    "divx",
    "divy",
    "divz",
]


path_in = os.path.join("system", "input_make_blockMeshDict.py")
if not os.path.exists(path_in):
    raise ValueError("No file " + path_in)

d = run_path(path_in, {})

for key in keys_input:
    if key not in d:
        raise ValueError(
            "The variable " + key + "has to be defined in the file " + path_in
        )

default_params = {"scale": 1, "ygrading": 1}

for key, value in default_params.items():
    if key not in d:
        d[key] = value


foam_v = os.environ["WM_PROJECT_VERSION"]
txt = f"""
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  {foam_v}                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
"""
txt += """
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
"""

txt += """
scale   {k};

vertices
(
    ( 0.0 0.0 0.0) // Vertex origo = 0
    ( {a} 0.0 0.0) // Vertex lengthx = 1
    ( {a} 0.0 {b}) // Vertex lengthxz = 2
    ( 0.0 0.0 {b}) // Vertex lengthz = 3

    ( 0.0 {h} 0.0) // Vertex heigthy = 4
    ( {a} {h} 0.0) // Vertex heightx = 5
    ( {a} {h} {b}) // Vertex heightxz = 6
    ( 0.0 {h} {b}) // Vertex heightz = 7

    ( {c} 0.0 0.0) // Vertex mountorigo = 8
    ( {e} 0.0 0.0) // Vertex mountlengthx = 9
    ( {e} 0.0 {b}) // Vertex mountlengthxz = 10
    ( {c} 0.0 {b}) // Vertex mountlengthz = 11

    ( {c} {mh} 0.0) // Vertex mountheigthy = 12
    ( {e} {mh} 0.0) // Vertex mountheightx = 13
    ( {e} {mh} {b}) // Vertex mountheightxz = 14
    ( {c} {mh} {b}) // Vertex mountheightz = 15

);


""".format(
    b=d["width"],
    h=d["height"],
    a=d["length"],
    c=d["mount_pos"] - 0.15,
    e=d["mount_pos"] + 0.15,
    mh=d["mount_h"],
    k=d["scale"],
)


txt += """
blocks
(
    //square block
    hex (0 1 5 4 3 2 6 7) ({divx} {divy} {divz}) simpleGrading (1 {ygrading} 1)

    //mountain
    hex (8 9 10 11 12 13 14 15) ({divx} {divy} {divz}) simpleGrading (1 {ygrading} 1)
);

""".format(
    divx=d["divx"], divy=d["divy"], divz=d["divz"], ygrading=d["ygrading"]
)

txt += """
boundary
(
    top
    {
        type wall;
        faces
        (
            (4 5 6 7)
        );
    }

    bottom
    {
        type wall;
        faces
        (
            (0 1 2 3)
        );
    }
    mountainsides
    {
        type wall;
        faces
        (
            (9 10 14 13)
	        (8 11 15 12)
        );
    }

    mountaintop
    {
        type wall;
        faces
        (
            (12 13 14 15)
        );
    }

    outlet
    {
        type cyclic;
        neighbourPatch inlet;
        faces
        (
            (1 5 6 2)
        );
    }

    inlet
    {
        type cyclic;
        neighbourPatch outlet;
        faces
        (
            (0 3 7 4)
        );
    }

    frontandbackplanes
    {
        type empty;
        faces
        (
            (3 2 6 7)
            (0 1 5 4)
            (8 9 13 12)
            (11 10 14 15)
        );
    }
);

// ************************************************************************* //
"""

print(txt)


path = os.path.join("system", "blockMeshDict")
print("save new blockMeshDict in path\n" + path)

with open(path, "w") as f:
    f.write(txt)
