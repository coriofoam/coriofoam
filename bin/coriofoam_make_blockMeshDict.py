#!/usr/bin/env python

import os
from math import sin, cos, pi
from runpy import run_path

keys_input = ["r", "height", "l2", "divx", "divy", "divz", "divr"]


path_in = os.path.join("system", "input_make_blockMeshDict.py")
if not os.path.exists(path_in):
    raise ValueError("No file " + path_in)

d = run_path(path_in, {})

for key in keys_input:
    if key not in d:
        raise ValueError(
            "The variable " + key + "has to be defined in the file " + path_in
        )

default_params = {"scale": 1, "zgrading": 50}

for key, value in default_params.items():
    if key not in d:
        d[key] = value


foam_v = os.environ["WM_PROJECT_VERSION"]
txt = f"""
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  {foam_v}                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
"""
txt += """
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
"""

txt += """
scale   {k};

vertices
(
( {l2} {l2} 0.0) // NorthEstsq = 0
(-{l2} {l2} 0.0) // NorthWestsq = 1
(-{l2} -{l2} 0.0) // SouthWestsq = 2
( {l2} -{l2} 0.0) // SouthEstsq = 3

( {a} {a} 0.) // NorthEst = 4
(-{a} {a} 0.) // NorthWest = 5
(-{a} -{a} 0.) // SouthWest = 6
( {a} -{a} 0.) // SouthEst = 7

( {l2} {l2} {h}) // NorthEstsqt = 8
(-{l2} {l2} {h}) // NorthWestsqt = 9
(-{l2} -{l2} {h}) // SouthWestsqt = 10
( {l2} -{l2} {h}) // SouthEstsqt = 11

( {a} {a} {h}) // NorthEstsqt = 12
(-{a} {a} {h}) // NorthWestsqt = 13
(-{a} -{a} {h}) // SouthWestsqt = 14
( {a} -{a} {h}) // SouthEstsqt = 15
);

""".format(
    l2=d["l2"], h=d["height"], a=d["r"] * cos(pi / 4), k=d["scale"]
)


txt += """
blocks
(
    //square block
    hex (9 8 11 10 1 0 3 2) ({divx} {divy} {divz}) simpleGrading (1 1 {zgrading})

    //slice1
    hex (13 12 8 9 5 4 0 1) ({divx} {divr} {divz}) simpleGrading (1 1 {zgrading})

    //slice2
    hex (9 10 14 13 1 2 6 5) ({divx} {divr} {divz}) simpleGrading (1 1 {zgrading})

    //slice3
    hex (10 11 15 14 2 3 7 6) ({divx} {divr} {divz}) simpleGrading (1 1 {zgrading})

    //slice4
    hex (11 8 12 15 3 0 4 7) ({divx} {divr} {divz}) simpleGrading (1 1 {zgrading})

);
""".format(
    divx=d["divx"],
    divy=d["divx"],
    divz=d["divz"],
    divr=d["divr"],
    zgrading=d["zgrading"],
)


txt += """
//create the quarter circles
edges
(
    arc 4 5 (0.0 {r} 0.0)
    arc 5 6 (-{r} 0.0 0.0)
    arc 6 7 (0.0 -{r} 0.0)
    arc 7 4 ({r} 0.0 0.0)

    arc 12 13 (0.0 {r} {h})
    arc 13 14 (-{r} 0.0 {h})
    arc 14 15 (0.0 -{r} {h})
    arc 15 12 ({r} 0.0 {h})

);
""".format(
    r=d["r"], h=d["height"]
)

txt += """
patches
(

    wall surface
    (
        (8 11 10 9)
        (8 12 15 11)
        (12 8 9 13)
        (9 10 14 13)
        (11 15 14 10)
    )

    wall bottom
    (
        (0 3 2 1)
        (0 4 7 3)
        (4 0 1 5)
        (1 2 6 5)
        (3 7 6 2)
    )

    wall walls
    (
        (5 4 12 13)
        (5 13 14 6)
        (6 14 15 7)
        (7 15 12 4)
    )

);

// ************************************************************************* //
"""

print(txt)

path = os.path.join("system", "blockMeshDict")
print("save new blockMeshDict in path\n" + path)

with open(path, "w") as f:
    f.write(txt)
