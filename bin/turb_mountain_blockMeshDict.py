#!/usr/bin/env python

import os
from math import sin, cos, pi, sqrt, ceil
from runpy import run_path

keys_input = ["length", "width", "height", "divx", "divy", "divz"]


path_in = os.path.join("system", "input_make_blockMeshDict.py")
if not os.path.exists(path_in):
    raise ValueError("No file " + path_in)

d = run_path(path_in, {})

for key in keys_input:
    if key not in d:
        raise ValueError(
            "The variable " + key + "has to be defined in the file " + path_in
        )

default_params = {"scale": 1, "ygrading": 1}

for key, value in default_params.items():
    if key not in d:
        d[key] = value


foam_v = os.environ["WM_PROJECT_VERSION"]
txt = f"""
/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  {foam_v}                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
"""
txt += """
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
"""

txt += """
scale   {k};

vertices
(
    ( 0.0 0.0 0.0) // Vertex fiveoclocksqb = 0
    ( 0.5 0.0 0.0) // Vertex fiveoclockcb = 1
    ( 1.5 0.0 0.0) // Vertex fiveoclockcb = 2
    ( {a} 0.0 0.0) // Vertex fiveoclocksqb = 3
    ( {a} 0.0 {b}) // Vertex fiveoclockcb = 4
    ( 1.5 0.0 {b}) // Vertex fiveoclockcb = 5
    ( 0.5 0.0 {b}) // Vertex fiveoclockcb = 6
    ( 0.0 0.0 {b}) // Vertex fiveoclockcb = 7
    ( 0.0 {h} 0.0) // Vertex fiveoclocksqb = 8
    ( 0.5 {h} 0.0) // Vertex fiveoclockcb = 9
    ( 1.5 {h} 0.0) // Vertex fiveoclockcb = 10
    ( {a} {h} 0.0) // Vertex fiveoclocksqb = 11
    ( {a} {h} {b}) // Vertex fiveoclockcb = 12
    ( 1.5 {h} {b}) // Vertex fiveoclockcb = 13
    ( 0.5 {h} {b}) // Vertex fiveoclockcb = 14
    ( 0.0 {h} {b}) // Vertex fiveoclockcb = 15

);


""".format(
    b=d["width"], h=d["height"], a=d["length"], k=d["scale"]
)


txt += """
blocks
(

    hex (0 1 9 8 7 6 14 15) ({divx1} {divy} {divz}) simpleGrading (1 ((0.1 0.25 41.9) (0.9 0.75 1)) 1)
    hex (1 2 10 9 6 5 13 14) (50 {divy} {divz}) simpleGrading (1 ((0.1 0.25 41.9) (0.9 0.75 1)) 1)
    hex (2 3 11 10 5 4 12 13) (225 {divy} {divz}) simpleGrading (1 ((0.1 0.25 41.9) (0.9 0.75 1)) 1)
);

""".format(
    divx1=ceil(d["divx"] / 6),
    divy=d["divy"],
    divz=d["divz"],
    ygrading=d["ygrading"],
)
txt += """
//create the mountain profil
edges
(
    spline 1 2 ((0.6 0.0124 0.0)(0.7 0.0395 0.0)(0.8 0.0724 0.0)(0.9 0.132 0.0)(1 0.172 0.0)(1.1 0.132 0.0)(1.2 0.0724 0.0)(1.3 0.0395 0.0)(1.4 0.0124 0.0))
    spline 6 5 ((0.6 0.0124 {b})(0.7 0.0395 {b})(0.8 0.0724 {b})(0.9 0.132 {b})(1 0.172 {b})(1.1 0.132 {b})(1.2 0.0724 {b})(1.3 0.0395 {b})(1.4 0.0124 {b}))
);""".format(
    b=d["width"]
)

txt += """
boundary
(
    top
    {
        type wall;
        faces
        (
            (8 15 14 9)
            (9 14 13 10)
            (10 13 12 11)
        );
    }

    bottom
    {
        type wall;
        faces
        (
            (0 1 6 7)
            (1 2 5 6)
            (2 3 4 5)
        );
    }

    outlet
    {
        type cyclic;
        neighbourPatch inlet;
        faces
        (
            (3 11 12 4)
        );
    }

    inlet
    {
        type cyclic;
        neighbourPatch outlet;
        faces
        (
            (0 7 15 8)
        );
    }

    frontandbackplanes
    {
        type empty;
        faces
        (
            (0 8 9 1)
            (1 9 10 2)
            (2 10 11 3)
            (7 6 14 15)
            (6 5 13 14)
	        (5 4 12 13)
        );
    }
);

// ************************************************************************* //
"""


print(txt)


path = os.path.join("system", "blockMeshDict")
print("save new blockMeshDict in path\n" + path)

with open(path, "w") as f:
    f.write(txt)
