{
    alphat = turbulence->nut()/Prt;
    alphat.correctBoundaryConditions();

    volScalarField alphaEff("alphaEff", turbulence->nu()/Pr + alphat);

    fvScalarMatrix rhokEqn
    (
        fvm::ddt(rhok)
      + fvm::div(phi, rhok)
      - fvm::laplacian(alphaEff, rhok)
     ==
        fvOptions(rhok)
    );

    rhokEqn.relax();

    fvOptions.constrain(rhokEqn);

    rhokEqn.solve();

    fvOptions.correct(rhok);
}
