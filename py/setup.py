from setuptools import setup, find_packages

setup(
    name='coriofoam',
    packages=find_packages(exclude=['tutorials', 'bin']))
