"""Script plotting the ux contour in center plane

First, one has to produce files with the cell centers by running::

  writeCellCentres -time 0

For parallel runs, one first has to reconstructed the fields with the command::

  reconstructPar -time 50

Can be used in ipython with the option --matplotlib or directly as a Python
script.

To launch ipython::

  ipython --matplotlib

In ipython::

  run plot_crosssection.py

Or directly with python::

  python plot_crosssection.py


"""

import sys

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.tri as tri

import pyof
# path to find the reconstructed times
path_case = """/.fsdyn_people/rogivue8sa/project/17CORIOFOAM/Data/Simulations/spinup_strat_mod
"""

# read mesh
print('Read mesh...')
sys.stdout.flush()
xm, ym, zm = pyof.readmesh(path_case + '/0')
xc, yc, zc = pyof.readmesh(path_case + '/0')
xe, ye, ze = pyof.readmesh(path_case + '/0')
print(' done')

# middle point
ym_unique = np.unique(ym)
ym_unique.sort()
dy = abs(np.diff(ym_unique).max())

y0m = 0.
y0m = ym_unique[abs(ym_unique - y0m).argmin()]
cond_m = (y0m - dy < ym) & (ym < y0m + dy) & (xm > 0) & (zm < 0.25) 
xm_2d = xm[cond_m]
zm_2d = zm[cond_m]

xm_unique = np.unique(xm[cond_m])
xm_unique.sort()
dx = abs(np.diff(xm_unique).max())
x0m = 4.
x0m = xm_unique[abs(xm_unique - x0m).argmin()]


cond1 = ((x0m - dx/2. < xm) & (xm < x0m + dx/2.) &
         (y0m - dy/2. < ym) & (ym < y0m + dy/2.) & (zm < 0.3))
zm_1d = zm[cond1]

#center point
yc_unique = np.unique(yc)
yc_unique.sort()
dy = abs(np.diff(yc_unique).max())

y0c = 0.
y0c = yc_unique[abs(yc_unique - y0c).argmin()]
cond_c = (y0c - dy < yc) & (yc < y0c + dy) & (xc > 0) & (zc < 0.25) 
xc_2d = xc[cond_c]
zc_2d = zc[cond_c]

xc_unique = np.unique(xc[cond_c])
xc_unique.sort()
dx = abs(np.diff(xc_unique).max())
x0c = 0.5
x0c = xc_unique[abs(xc_unique - x0c).argmin()]


cond2 = ((x0c - dx/2. < xc) & (xc < x0c + dx/2.) &
         (y0c - dy/2. < yc) & (yc < y0c + dy/2.) & (zc < 0.3))
zc_1d = zc[cond2]

triang = tri.Triangulation(xc_2d, zc_2d)

#edge point
ye_unique = np.unique(ye)
ye_unique.sort()
dy = abs(np.diff(ye_unique).max())

y0e = 0.
y0e = ye_unique[abs(ye_unique - y0e).argmin()]
cond_e = (y0e - dy < ye) & (ye < y0e + dy) & (xe > 0) & (ze < 0.25) 
xe_2d = xe[cond_e]
ze_2d = ze[cond_e]

xe_unique = np.unique(xe[cond_e])
xe_unique.sort()
dx = abs(np.diff(xe_unique).max())
x0e = 6.
x0e = xe_unique[abs(xe_unique - x0e).argmin()]


cond3 = ((x0e - dx/2. < xe) & (xe < x0e + dx/2.) &
         (y0e - dy/2. < ye) & (ye < y0e + dy/2.) & (ze < 0.3))
ze_1d = ze[cond3]

triang = tri.Triangulation(xe_2d, ze_2d)

# read data
key = 'uy'
timename = '10'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field = u[0]
    elif key == 'uy':
        field = u[1]
    elif key == 'uz':
        field = u[2]
    else:
        raise ValueError
else:
    field = pyof.readscalar(timename, key)


f_2d = field[cond_m]

# paht to find the reconstructed times
path_case = """/.fsdyn_people/rogivue8sa/project/17CORIOFOAM/Data/Simulations/spinup_strat_mod
"""


# make the figures
fig1 = plt.figure()
ax1 = plt.gca()
plt.plot(field[cond1], zm_1d, 'k^-')
lmiddle, lcenter, ledge = plt.plot(field[cond1], zm_1d, 'k^-',field[cond2], zc_1d, 'r^-',field[cond3], ze_1d, 'c^-')
ax1.set_ylabel(r'$z$ (m)')
ax1.set_xlabel(r'Uy (m/s)')
ax1.set_title(r'{} at $t = {}$ and $y = {:.4f}$ m'.format(key, timename, y0m))

fig = plt.figure()
ax = plt.gca()

t = ax.tricontourf(triang, f_2d, 30)
ax.set_aspect('equal')
ax.set_title(r'{} at $t = {}$ and $y = {:.4f}$ m'.format(key, timename, y0m))
ax.set_ylabel(r'$z$ (m)')
ax.set_xlabel(r'$x$ (m)')
plt.colorbar(t)

#plt.savefig('../../../../Data/Figures'
#            '/spinup_strat_komega_t80_water.png'.format(key, timename, y0))

plt.show()
