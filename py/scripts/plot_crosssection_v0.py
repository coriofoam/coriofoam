"""Script plotting the ux contour in center plane

First, one has to produce files with the cell centers by running::

  writeCellCentres -time 0

For parallel runs, one first has to reconstructed the fields with the command::

  reconstructPar -time 200

Can be used in ipython with the option --matplotlib or directly as a Python
script.

To launch ipython::

  ipython --matplotlib

In ipython::

  run plot_crosssection.py

Or directly with python::

  python plot_crosssection.py


"""

import sys

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.tri as tri

import fluidfoam

# parameters
path_case = '/home/users/clemenco1q/DEV/coriofoam/runs/2Dtest/'
y0 = 0.5
key = 'rhok'
# key = 'uz'
timename = '30'
NX = 300
NY = 50
NZ = 1

# read mesh
print('Read mesh...', end='')
sys.stdout.flush()
xm, ym, zm = fluidfoam.readmesh(path_case, [NX, NY, NZ])

if key.startswith('u'):
    u = fluidfoam.readvector(path_case, timename, 'U', [NX, NY, NZ])
    if key == 'ux':
        field = u[0]
    elif key == 'uy':
        field = u[1]
    elif key == 'uz':
        field = u[2]
    else:
        raise ValueError
else:
    field = fluidfoam.readscalar(path_case, timename, key, [NX, NY, NZ])




# make the figure
fig = plt.figure()

t = plt.plot(ym[1,:,0], field[150,:,0])
#t = ax.tricontourf(triang, f_2d, timename)
# ax.set_aspect('equal')
plt.title(r'{} at $t = {}$ and $y = {:.4f}$ m'.format(key, timename, y0))
plt.ylabel(r'$z$ (m)')
plt.xlabel(r'$x$ (m)')


# plt.savefig('fig_2d_{}_t={}_y={:.4f}.png'.format(key, timename, y0))

plt.show()
