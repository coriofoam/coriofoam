"""Script plotting the ux contour in center plane

First, one has to produce files with the cell centers by running::

  writeCellCentres -time 0

For parallel runs, one first has to reconstructed the fields with the command::

  reconstructPar -time 50

Can be used in ipython with the option --matplotlib or directly as a Python
script.

To launch ipython::

  ipython --matplotlib

In ipython::

  run plot_crosssection.py

Or directly with python::

  python plot_evolution.py


"""

import sys

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.tri as tri

import fluidfoam
path_case = '/fsnet/project/meige/2017/17CORIOFOAM/Data/Simulations/spinup_strat'

# read mesh
print('Read mesh...')
sys.stdout.flush()
xm, ym, zm = fluidfoam.readmesh(path_case + '/0')
print(' done')

ym_unique = np.unique(ym)
ym_unique.sort()
dy = abs(np.diff(ym_unique).max())

y0 = 0.
y0 = ym_unique[abs(ym_unique - y0).argmin()]
cond = (y0 - dy < ym) & (ym < y0 + dy)
xm_2d = xm[cond]
zm_2d = zm[cond]

xm_unique = np.unique(xm[cond])
xm_unique.sort()
dx = abs(np.diff(xm_unique).max())
x0 = 4.
x0 = xm_unique[abs(xm_unique - x0).argmin()]


cond1 = ((x0 - dx/2. < xm) & (xm < x0 + dx/2.) &
         (y0 - dy/2. < ym) & (ym < y0 + dy/2.))
zm_1d = zm[cond1]

triang = tri.Triangulation(xm_2d, zm_2d)




timelist_p=os.listdir('/fsnet/project/meige/2017/17CORIOFOAM/Data/Simulations/spinup_strat')
for i in range len(timelist)

if  timelist_f=


timename=[20,50,100,200]



# read data 20

key = 'uy'
timename_loop = timename[i]

if key.startswith('u'):
    u = fluidfoam.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_twenty = u[0]
    elif key == 'uy':
        field_twenty = u[1]
    elif key == 'uz':
        field_twenty = u[2]
    else:
        raise ValueError
else:
    field_twenty = fluidfoam.readscalar(timename, key)


f_2d_twenty = field_twenty[cond]

# read data 50
key = 'uy'
timename = '50'

if key.startswith('u'):
    u = fluidfoam.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_fifty = u[0]
    elif key == 'uy':
        field_fifty = u[1]
    elif key == 'uz':
        field_fifty = u[2]
    else:
        raise ValueError
else:
    field_fifty = fluidfoam.readscalar(timename, key)


f_2d_fifty = field_fifty[cond]

# read data 100
key = 'uy'
timename = '100'

if key.startswith('u'):
    u = fluidfoam.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_onehundred = u[0]
    elif key == 'uy':
        field_onehundred = u[1]
    elif key == 'uz':
        field_onehundred = u[2]
    else:
        raise ValueError
else:
    field_onehundred = fluidfoam.readscalar(timename, key)


f_2d_onehundred = field_onehundred[cond]

# read data 200
key = 'uy'
timename = '200'

if key.startswith('u'):
    u = fluidfoam.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_twohundred = u[0]
    elif key == 'uy':
        field_twohundred = u[1]
    elif key == 'uz':
        field_twohundred = u[2]
    else:
        raise ValueError
else:
    field_twohundred = fluidfoam.readscalar(timename, key)


f_2d_twohundred = field_twohundred[cond]

path_case = '../Data/Simulations/spinup_strat'



# make the figure
fig1 = plt.figure()
ax1 = plt.gca()
l20, l50, l100, l200 = plt.plot(field_twenty[cond1], zm_1d, 'k^-', field_fifty[cond1], zm_1d, 'b^-',field_onehundred[cond1], zm_1d, 'c^-',field_twohundred[cond1], zm_1d, 'm^-')
ax1.set_ylabel(r'$z$ (m)')
ax1.set_xlabel(r'Uy (m/s)')
ax1.set_title(r'Velocity Evolution'.format(key, timename, y0))
plt.legend([l20, l50, l100, l200], ['20', '50', '100', '200'])

fig = plt.figure()
ax = plt.gca()

t = ax.tricontourf(triang, f_2d, 30)
# ax.set_aspect('equal')
ax.set_title(r'{} at $t = {}$ and $y = {:.4f}$ m'.format(key, timename, y0))
ax.set_ylabel(r'$z$ (m)')
ax.set_xlabel(r'$x$ (m)')
plt.colorbar(t)

plt.savefig('/.fsdyn_people/lavaud5ni/project/17CORIOFOAM/Data/Figures'
            '/spinup_strat_komega_t80_water.png'.format(key, timename, y0))

plt.show()
