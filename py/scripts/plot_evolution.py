"""Script plotting the ux contour in center plane

First, one has to produce files with the cell centers by running::

  writeCellCentres -time 0

For parallel runs, one first has to reconstructed the fields with the command::

  reconstructPar -time 50

Can be used in ipython with the option --matplotlib or directly as a Python
script.

To launch ipython::

  ipython --matplotlib

In ipython::

  run plot_crosssection.py

Or directly with python::

  python plot_evolution.py


"""

import sys

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.tri as tri

import pyof
# paht to find the reconstructed times
path_case = '../../../../Data/Simulations/spinup_strat_mod'

# read mesh
print('Read mesh...')
sys.stdout.flush()
xm, ym, zm = pyof.readmesh(path_case + '/0')
print(' done')

ym_unique = np.unique(ym)
ym_unique.sort()
dy = abs(np.diff(ym_unique).max())

y0 = 0.
y0 = ym_unique[abs(ym_unique - y0).argmin()]
cond = (y0 - dy < ym) & (ym < y0 + dy)
xm_2d = xm[cond]
zm_2d = zm[cond]

xm_unique = np.unique(xm[cond])
xm_unique.sort()
dx = abs(np.diff(xm_unique).max())
x0 = 4.
x0 = xm_unique[abs(xm_unique - x0).argmin()]


cond1 = ((x0 - dx/2. < xm) & (xm < x0 + dx/2.) &
         (y0 - dy/2. < ym) & (ym < y0 + dy/2.))
zm_1d = zm[cond1]

triang = tri.Triangulation(xm_2d, zm_2d)

# read data 20
key = 'uy'
timename = '20'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_twenty = u[0]
    elif key == 'uy':
        field_twenty = u[1]
    elif key == 'uz':
        field_twenty = u[2]
    else:
        raise ValueError
else:
    field_twenty = pyof.readscalar(timename, key)


f_2d_twenty = field_twenty[cond]

# read data 50
key = 'uy'
timename = '50'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_fifty = u[0]
    elif key == 'uy':
        field_fifty = u[1]
    elif key == 'uz':
        field_fifty = u[2]
    else:
        raise ValueError
else:
    field_fifty = pyof.readscalar(timename, key)


f_2d_fifty = field_fifty[cond]

# read data 100
key = 'uy'
timename = '100'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_onehundred = u[0]
    elif key == 'uy':
        field_onehundred = u[1]
    elif key == 'uz':
        field_onehundred = u[2]
    else:
        raise ValueError
else:
    field_onehundred = pyof.readscalar(timename, key)


f_2d_onehundred = field_onehundred[cond]

# read data 200
key = 'uy'
timename = '200'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_twohundred = u[0]
    elif key == 'uy':
        field_twohundred = u[1]
    elif key == 'uz':
        field_twohundred = u[2]
    else:
        raise ValueError
else:
    field_twohundred = pyof.readscalar(timename, key)


f_2d_twohundred = field_twohundred[cond]

# read data 400
key = 'uy'
timename = '400'

if key.startswith('u'):
    u = pyof.readvector(path_case, timename, 'U')
    if key == 'ux':
        field_fourhundred = u[0]
    elif key == 'uy':
        field_fourhundred = u[1]
    elif key == 'uz':
        field_fourhundred = u[2]
    else:
        raise ValueError
else:
    field_fourhundred = pyof.readscalar(timename, key)


f_2d_fourhundred = field_fourhundred[cond]
# paht to find the reconstructed times
path_case = '../../../../Data/Simulations/spinup_strat_mod'



# make the figure
fig1 = plt.figure()
ax1 = plt.gca()
l20, l50, l100, l200, l400 = plt.plot(field_twenty[cond1], zm_1d, 'k^-', field_fifty[cond1], zm_1d, 'b^-',field_onehundred[cond1], zm_1d, 'c^-',field_twohundred[cond1], zm_1d, 'm^-', field_fourhundred[cond1], zm_1d, 'r^-')
ax1.set_ylabel(r'$z$ (m)')
ax1.set_xlabel(r'Uy (m/s)')
ax1.set_title(r'Velocity Evolution'.format(key, timename, y0))
plt.legend([l20, l50, l100, l200, l400], ['20', '50', '100', '200', '400'])


#plt.savefig('/spinup_strat_komega_t80_water.png'.format(key, timename, y0))

plt.show()
# make the figure
fig2 = plt.figure()
ax1 = plt.gca()

r0= np.sqrt(x0**2 + y0**2)
U0= r0*0.2

l20, l50, l100, l200, l400 = plt.plot(abs(field_twenty[cond1]-U0), zm_1d, 'k^-', abs(field_fifty[cond1]-U0), zm_1d, 'b^-',abs(field_onehundred[cond1]-U0), zm_1d, 'c^-',abs(field_twohundred[cond1]-U0), zm_1d, 'm^-', abs(field_fourhundred[cond1]-U0), zm_1d, 'r^-')
ax1.set_ylabel(r'$z$ (m)')
ax1.set_xlabel(r'Uy (m/s)')
ax1.set_title(r'Velocity Evolution'.format(key, timename, y0))
plt.legend([l20, l50, l100, l200, l400], ['20', '50', '100', '200', '400'])

#plt.savefig('/spinup_strat_komega_t80_water.png'.format(key, timename, y0))

plt.show()
