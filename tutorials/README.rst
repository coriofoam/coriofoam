Tutorials
=========

To run one of the tutorials, copy the corresponding directory, go into the new
directory and read the README.txt file::

  cp -r spinup_strat/ ../runs/spinup_strat_$$
  cd ../runs/spinup_strat_$$
  cat README.rst
