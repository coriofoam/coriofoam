Tutorial gravitational_instability_norot
========================================

Different parameters can be modified in some files, in particular:

- constant/polyMesh/blockMeshDict
- system/funkySetFieldsDict
- system/decomposeParDict

Do not modify the files in the directories coriofoam/tutorials/ unless you
really know what you do. When you want to modify the parameters, copy the
tutorial directory in the directory coriofoam/runs with a proper name (for
example with the date and time) and then modify these files.

Depending on how you want to run the simulation, different commands have to be
launched. The commands are in bash files corresponding to different steps:

- clean.sh
- init_simul.sh
- init_parallel.sh (only for parallel runs)
- launch_sequencial.sh
- launch_parallel_local.sh
- launch_parallel_cluster.sh

Before running these files, have a look at the code and the explanations in the
file. You can use `cat` like this::

  cat clean.sh

Sequential simulation
---------------------

.. code:: bash

   ./clean.sh
   ./init_simul.sh
   ./launch_sequencial.sh

For parallel simulations, first write a correct number of cores in the file
system/decomposeParDict (line `numberOfSubdomains 16;`)

Parallel simulation on your local machine
-----------------------------------------

Do not do this on the frontal of the cluster!

.. code:: bash

   ./clean.sh
   ./init_simul.sh
   ./init_parallel.sh
   ./launch_parallel_local.sh

Parallel simulation on the cluster
----------------------------------

.. code:: bash

  ./clean.sh
  ./init_simul.sh
  ./init_parallel.sh
  ./launch_parallel_cluster.sh

Visualize the results with paraview
-----------------------------------

.. code:: bash

   paraview empty_for_paraview &

Choose the second OpenFOAM entry in the window "Data Open With...".

In the left subwindow, choose "Case type": "Decomposed Case" and click on Apply.
