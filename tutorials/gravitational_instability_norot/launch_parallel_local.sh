#!/usr/bin/env bash

NBCORES=$(grep numberOfSubdomains system/decomposeParDict |\
		 awk '{print $2}' | cut -f 1 -d ";")

mpirun -np ${NBCORES} coriofoam -parallel > log.txt &

