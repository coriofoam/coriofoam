#!/usr/bin/env python

import os
from math import sin, cos, pi, sqrt
from runpy import run_path

keys_input = ['r', 'b', 'height', 'divx', 'divy', 'divz', 'divr']


path_in = os.path.join('system', 'input_make_blockMeshDict.py')
if not os.path.exists(path_in):
    raise ValueError('No file ' + path_in)

d = run_path(path_in, {})

for key in keys_input:
    if key not in d:
        raise ValueError('The variable ' + key +
                         'has to be defined in the file ' + path_in)

default_params = {'converttometer': 1, 'zgrading': 1}

for key, value in default_params.items():
    if key not in d:
        d[key] = value


txt = '''
FoamFile
{
version 2.0;
format ascii;
class dictionary;
object blockMeshDict;
}
'''

txt += '''
convertToMeters {k};

vertices
(
    ( 0.0 0.0 0.0) // Vertex fiveoclocksqb = 0
    ( {a} {b} 0.) // Vertex fiveoclockcb = 1
    ( {a} -{b} 0.) // Vertex fiveoclockcb = 2

    ( 0.0 0.0 {h}) // Vertex fiveoclocksqb = 3
    ( {a} {b} {h}) // Vertex fiveoclockcb = 4
    ( {a} -{b} {h}) // Vertex fiveoclockcb = 5
);


'''.format(b=d['b'], h=d['height'], a=sqrt(d['r']*d['r']- d['b']*d['b']), k=d['converttometer'])


txt += '''
blocks
(
    //square block
    hex (
    4 3 0 1
    5 3 0 2
    )
    ({divx} {divz} {divy})
    simpleGrading (1 {zgrading} 1)
);

'''.format(divx=d['divx'], divy=d['divy'], divz=d['divz'], divr=d['divr'],
           zgrading=d['zgrading'])


txt += '''
//create the quarter circles
edges
(
    arc 2 1 ({r} 0.0 0.0)
    arc 5 4 ({r} 0.0 {h})
);'''.format(r=d['r'], h=d['height'])

txt += '''
boundary
(
    walls
    {
        type wall;
        faces
        (
            (4 5 2 1)
        );
    }

    surface
    {
        type wall;
        faces
        (
            (4 5 3 3)
        );
    }

    bottom
    {
        type wall;
        faces
        (
            (1 2 0 0)
        );
    }

    up
    {
        type cyclic;
        neighbourPatch down;
        faces
        (
            (3 4 1 0)
        );
    }

    down
    {
        type cyclic;
        neighbourPatch up;
        faces
        (
            (0 2 5 3)
        );
    }

    axis
    {
        type empty;
        faces
        (
            (0 0 3 3)
        );
    }
);'''

print(txt)

path = os.path.join('system', 'blockMeshDict')
print('save new blockMeshDict in path\n' + path)

with open(path, 'w') as f:
    f.write(txt)

