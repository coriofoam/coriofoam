"""Input file for the command coriofoam_make_blockMeshDict.py"""

r = 6.5
height = 0.5
b = 0.25
divx = 80
divy = 80
divz = 20
divr = 50

zgrading = '((0.99 0.5 1) (0.01 0.5 0.2))'
