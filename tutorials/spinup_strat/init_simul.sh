
# force bash script to stop if there is an error
set -e

# make the file blockMesh with a python script
coriofoam_make_blockMeshDict.py

# blockMesh makes the mesh grid using the file constant/polyMesh/blockMeshDict
# it creates other files in constant/polyMesh
blockMesh

# ??
topoSet
setsToZones

# we copy the directory 0_init into the directory 0 which is read by OpenFoam
# to set the initial state of the simulation
cp -rf 0_init 0

# We use the program funkySetFields to modify the initial state funkySetFields
# take the file system/funkySetFieldsDict as input and modify the directory 0
funkySetFields -time 0

echo "end of initialization (you may have to initialize the parallel computing)."
